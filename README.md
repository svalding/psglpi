# PSGLPI #

This Powershell module uses the RESTful API of GLPI to create tickets in the system.
This is helpful for monitoring services and other metrics in your production environment.

The Module is cross-platform, meaning it runs in Windows Powershell, and Powershell Core 6.0.2

Tested on Windows 10, Debian 8, and Mac OS X.

Example usage and full documentation on the code can be found by running:

Get-Help New-GLPITicket -Full in a powershell window once the module has been installed.
