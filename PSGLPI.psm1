function New-GLPISessionToken {
    #Region Init Session

    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $Headers.Add("App-Token", "$AppToken")
    $Headers.Add("Authorization", "user_token $UserToken")

    $Session = Invoke-RestMethod -Uri "http://$GLPIServer/glpi/apirest.php/initSession" -Headers $Headers -Method Get -ContentType 'application/json'
    $script:Session_token = $Session.session_token


    #endregion

}

function Invoke-GLPITicket {
    #region Generate Ticket from Form Data

    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $Headers.Add("App-Token", "$AppToken")
    $Headers.Add("Session-Token", "$Session_token")

    #generate properties for ticket object
    $props = @{

        'name'    = "$Title"
        'content' = "$Description"
        'status'  = '1'
        'itilcategories_id' = "5"
        'users_id_lastupdater' = ''
       'users_id_recipient' = ''

    }

    #create ticket object with properties from hash table
    $ticketObj = New-Object psobject -Property $props

    #convert ticket object to json payload
    $ticketJson = $ticketObj | ConvertTo-Json -Compress


    $ticketJson = "{""input"": [$ticketJson]}"
    #Create ticket with API Call
    Invoke-RestMethod -Uri "http://$GLPIServer/glpi/apirest.php/Ticket" -Headers $Headers -ContentType 'application/json' -Method Post -Body $ticketJson
}
function New-GLPITicket {
    <#
        .SYNOPSIS
        Create a new ticket in GLPI using the RESTful API

        .PARAMETER GLPIServer
        String value of the FQDN of your GLPI server

        .PARAMETER AppToken
        The API token for your application in GLPI. This is found under:
        Setup > General > API
        You should add an API Client for each Server/Service this will run on.

        .PARAMETER UserToken
        The API token of the API user. This could be your GLPI account, or a service account.
        This is found under:
        Administration > Users > [username] > Settings > Remote Access Keys section > API Token

        .PARAMETER Title
        The ticket title. Accepts strings.

        .PARAMETER Description
        This is the Description area of a ticket, which translates to the 'content' section of the JSON payload.

        .EXAMPLE

        New-GLPITicket -GLPIServer 'glpi.mydomain.com' -AppToken '125xvsdf234' -UserToken '8675309eeeiiinnnn' -Title 'Toy's R Us is Doomed' -Description 'Always a toys r us kid!'

        .EXAMPLE

        $params = @{
            'GLPIServer' = 'glpi.mydomain.com'
            'AppToken' = '123456'
            'UserToken' = 'asdfqwer'
            'Title' = 'Some Title'
            'Description' = 'Here's some details. Blah Blah.'

        }

        New-GLPITicket @params

        .EXAMPLE

        New-GLPITicket -GLPIServer 'glpi.mydomain.com' -AppToken '125xvsdf234' -UserToken '8675309eeeiiinnnn' -Title 'Toy's R Us is Doomed' -Description "$(Get-EventLog -Logname Application -Newest 1 | Select-Object -ExpandProperty message)"

    #>

    Param(
        [cmdletBinding()]
        [Parameter(Mandatory, Position = 0 )]
        [string]$GLPIServer,
        [Parameter(Mandatory, Position = 1)]
        [string]$AppToken,
        [Parameter(Mandatory, Position = 2 )]
        [string]$UserToken,
        [Parameter(Mandatory, Position = 3 )]
        [string]$Title,
        [parameter(Mandatory, Position = 4 )]
        [string]$Description
    )


    New-GLPISessionToken #This function uses the $AppToken and $UserToken

    Invoke-GLPITicket #This function uses the $Title and Description


}

Function Get-GLPITicket {
    Param(
        [cmdletBinding()]
        [Parameter(Mandatory, Position = 0)]
        [string]$GLPIServer,
        [Parameter(Mandatory, Position = 1)]
        [string]$AppToken,
        [Parameter(Mandatory, Position = 2)]
        [string]$UserToken,
        [Parameter(Mandatory, Position = 3)]
        [string]$TicketID

    )


    New-GLPISessionToken

    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $Headers.Add("App-Token", "$AppToken")
    $Headers.Add("Session-Token", "$Session_token")

    Invoke-RestMethod -uri "http://$GLPIServer/glpi/apirest.php/Ticket/$TicketID`?expand_dropdowns=true" -Headers $Headers -Method Get -ContentType 'application/json'

}

Function Add-GLPIFollowup {

    [cmdletBinding()]
    Param(
        [parameter(Mandatory, Position = 0)]
        [string]$GLPIServer,
        [Parameter(Mandatory, Position = 1)]
        [string]$AppToken,
        [Parameter(Mandatory, Position = 2)]
        [string]$UserToken,
        [Parameter(Mandatory, Position = 3)]
        [string]$TicketID,
        [Parameter(Mandatory, Position = 4)]
        [string]$FollowupText
    )

    New-GLPISessionToken #This function uses the $AppToken and $UserToken

    $Headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
    $Headers.Add("App-Token", "$AppToken")
    $Headers.Add("Session-Token", "$Session_token")

    #generate properties for ticket object
    [pscustomobject]$ticketObj = @{

        'tickets_id'      = "$TicketID"
        'is_private'      = '0'
        'requesttypes_id' = '6'
        'content'         = "$FollowupText"

    }

    #convert ticket object to json payload
    $ticketJson = $ticketObj | ConvertTo-Json -Compress


    $ticketJson = "{""input"": [$ticketJson]}"
    #Create ticket with API Call
    Invoke-RestMethod -Uri "http://$GLPIServer/glpi/apirest.php/Ticket/$TicketID/TicketFollowup" -Headers $Headers -ContentType 'application/json' -Method Post -Body $ticketJson
}